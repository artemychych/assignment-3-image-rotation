#include "bmp.h"
#include "operations.h"
#include <stdbool.h>
#include <string.h>

char* read_states[] = {
    [READ_OK] = "File read succesfully \n",
    [READ_ERR] = "File read failed(\n"
};

char* write_states[] = {
    [WRITE_OK] = "File write succssfully\n",
    [WRITE_ERR] = "File write failed\n"
};

int main(int argc, char** argv) {
    if (argc == 4) {
        FILE* input_file = NULL;
        FILE* output_file = NULL;
        int rotate_count = 0;
        bool flag = true;
        if (strcmp(argv[3],"0") == 0) {
            rotate_count = 0;
        } else if (strcmp(argv[3], "90") == 0 || strcmp(argv[3], "270") == 0) {
            rotate_count = 3;
        } else if (strcmp(argv[3],"180") == 0 || strcmp(argv[3], "-180") == 0) {
            rotate_count = 2;
        } else if (strcmp(argv[3],"-90") == 0 || strcmp(argv[3], "270") == 0) {
            rotate_count = 1;
        } else {
            flag = false;
        }
            
        if (flag == true) {
            input_file = fopen(argv[1], "rb");
            if (input_file != NULL){
                fprintf(stderr, "%s", "Input file opened succesfully\n");
                output_file = fopen(argv[2], "wb");
                if(output_file != NULL){
                    fprintf(stderr, "%s", "Output file opened successfully\n");
                    struct image image = img_empty();
                    enum input_state input_state = read_from_bmp_file(input_file, &image);
                    fprintf(stderr, "%s", read_states[input_state]);
                    if (input_state == READ_OK) {
                        bool flag1 = true;
                        struct image new_img;
                        if (rotate_count == 1) {
                            new_img = rotate(image);
                        } else if (rotate_count == 2) {
                            new_img = rotate(image);
                            new_img = rotate(new_img);
                        } else if (rotate_count == 3) {
                            new_img = rotate(image);
                            new_img = rotate(new_img);
                            new_img = rotate(new_img);
                        } else {
                            flag1 = false;
                        }
                        
                    
                        
                        enum output_state output_state = write_to_bmp_file(output_file, &new_img);
                        fprintf(stderr, "%s", write_states[output_state]);
                        if (flag1) {
                            free(new_img.data);
                        }
                        free(image.data);
                    }
                    fclose(input_file);
                    fclose(output_file);
                } else {
                    fclose (input_file);
                    fprintf(stderr, "%s", "Output file opening error\n");
                }
            } else fprintf(stderr, "%s", "Input file opening error\n");
        } else fprintf(stderr, "%s", "Invalid value of angle, should be in [0, 90, 180, 270, -90, -180, -270]");
        
    } else fprintf(stderr, "%s", "Expected 4 arguments\n");
    return 0;
}
