#pragma once

#include "format.h"

struct image rotate(struct image const img);

struct image img_empty(void);

struct image img_create(uint32_t width, uint32_t height, struct pixel* data);
